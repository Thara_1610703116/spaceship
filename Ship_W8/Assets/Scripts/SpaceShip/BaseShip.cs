﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShip
{
    public abstract class BaseShip : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet,defaultBullet1;
        [SerializeField] protected Transform gunPosition, gunPosition1, gunPosition2;

        public int Hp { get; protected set; }
        public float Speed { get; protected set; }
        public Bullet Bullet { get; protected set; }
        
        protected void Init(int hp ,float speed, Bullet bullet) 
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
        }
        public abstract void Fire();
    }

}
