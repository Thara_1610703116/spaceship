﻿using System;
using UnityEngine;
namespace SpaceShip
{
    public class PlayerSpaceship : BaseShip, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerBoomSound;
        [SerializeField] private float playerBoomSoundVolume = 0.3f;
        

        void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(gunPosition1 != null, "gunPosition1 cannot be null");
            Debug.Assert(gunPosition2 != null, "gunPosition2 cannot be null");
            Debug.Assert(playerFireSound != null, "playerFireSound cannot be null");
        }
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerBoomSound, Camera.main.transform.position, playerBoomSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();

        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position,playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up); 
            var bullet1 = Instantiate(defaultBullet1, gunPosition1.position, Quaternion.identity);
            bullet1.Init(Vector2.up);
            var bullet2 = Instantiate(defaultBullet1, gunPosition2.position, Quaternion.identity);
            bullet2.Init(Vector2.up);

        }
        

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;

                
            }
            Explode();         
        }

    }
}

