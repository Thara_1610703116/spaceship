﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject enemyPrefab;
    GameObject currentEnemy;
    
    bool canSpawn = true;
    void Start()
    {
        Spawns();
    }

    void Update()
    {
        if (currentEnemy == null && canSpawn)
        {
            canSpawn = false;
            //Invoke("Spawns", 3);
        }
    }

    void Spawns()
    {
        currentEnemy = Instantiate(enemyPrefab, SpawnPos(), Quaternion.identity);
        canSpawn = true;
        print("Spawn()");
    }
    Vector3 SpawnPos()
    {
        //Vector2 randomSpawnPosition = new Vector2(Random.Range(-7.5f, 7.5f), Random.Range(-7.5f, 7.5f));
        //return new Vector3(Random.Range(5f, 5f), gameObject.transform.position.y);
        return new Vector2(Random.Range(-4f, 4f), Random.Range(-4f, 4f));
    }
}
