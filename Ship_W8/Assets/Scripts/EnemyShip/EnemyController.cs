﻿using Manager;
using SpaceShip;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        //[SerializeField] private PlayerSpaceship playerSpaceship;

        private PlayerSpaceship spawnedPlayerShip;

        //public void Init(PlayerSpaceship playerSpaceship)
        //{
        //    spawnedPlayerShip = playerSpaceship;
        //}
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
            
        }
        private void MoveToPlayer()
        {
            // TODO: Implement this later

            spawnedPlayerShip = GameManager.Instance.spawnedPlayerShip;
            //spawnedPlayerShip = FindObjectOfType<GameManager>().spawnedPlayerShip;
            if (spawnedPlayerShip == null)
            {
                print("spawnedPlayerShip == null");
                return;
            }
                
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
            

            if(distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);   
            }
            

        }
    }
}