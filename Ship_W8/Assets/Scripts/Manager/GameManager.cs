﻿using System;
using EnemyShip;
using Manager;
using SpaceShip;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        EnemySpaceship spaceship;
        bool canSpawn = true;
        public int score = 0;
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        [SerializeField] private SoundManager soundManager;

        
        public static GameManager Instance { get; private set; }
        
        public PlayerSpaceship spawnedPlayerShip;
        //private PlayerSpaceship spawnedEnemyShip;

        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            Debug.Assert(condition: playerSpaceshipHp != null, "playerSpaceshipHp cannot be null");
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            Debug.Assert(condition: playerSpaceshipMoveSpeed != null, "playerSpaceshipMoveSpeed cannot be null");
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            Debug.Assert(condition: enemySpaceshipHp != null, "enemySpaceshipHp cannot be null");
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            Debug.Assert(condition: enemySpaceshipMoveSpeed != null, "enemySpaceshipMoveSpeed cannot be null");
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(this);
            
            startButton.onClick.AddListener(OnStartButtonClicked);

            soundManager.PlayBGM();
            //soundManager.Playfire();
            //SoundManager.Instance.PlayBGM();

            if(SceneManager.GetActiveScene().name == "Space")
            {
                print("if name scene space");
                return;
            }
            

            SpawnPlayerSpaceship();

        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }
        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            //SpawnEnemySpaceship();
            //canSpawn = false;
        }
        private void SpawnPlayerSpaceship()
        {
            print("set SpawnPlayerSpaceship");
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnEnemySpaceshipExploded;
        }
        public void OnPlayerSpaceshipExploded()
        {
            Restart();
        }
        private void SpawnEnemySpaceship()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceship, SpawnPos(), Quaternion.identity);

            canSpawn = true;
            print("Spawn()");
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;

           
            //var enemyController = spawnedEnemyShip.GetComponent<EnemyController>();
            //enemyController.Init(spawnedPlayerShip);

        }
        private void OnEnemySpaceshipExploded()
        {
            score++;
            scoreManager.SetScore(score);
            //if (SceneManager.GetActiveScene().name == "World1")
            //{
            //    print("if name scene World1");
            //    return;
            //}
            Restart();
        }
        public void Restart()
        {
            
            if (SceneManager.GetActiveScene().name == "Space2")
            {
                print("if name scene space2");
                DestroyRemainingShip();
                dialog.gameObject.SetActive(true);
                OnRestarted?.Invoke();
                //SceneManager.LoadScene(0);

            }
           
            //SceneManager.LoadScene(0);
        }
        private void DestroyRemainingShip()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var Player in remainingPlayers)
            {
                Destroy(Player);
            }
        }
        public void Quit()
        {
            Application.Quit();
        }
        void Update()
        {
            if(spawnedPlayerShip == null)
            {
                spawnedPlayerShip = FindObjectOfType<PlayerSpaceship>();
            }
            if(dialog == null)
            {
                dialog = FindObjectOfType<UIManager>().dialog;
            }
            if(spaceship == null && canSpawn )
            {
                canSpawn = false;
                Invoke("SpawnEnemySpaceship", 4);
            }
            if(Input.GetKeyDown(KeyCode.Q))
            {
                UnityEngine.Application.Quit();
            }

        }
        Vector3 SpawnPos()
        {
            return new Vector3(UnityEngine.Random.Range(-3f, 5f), gameObject.transform.position.y);           
        }

    }
}

