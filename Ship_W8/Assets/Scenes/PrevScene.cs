﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PrevScene : MonoBehaviour
{
    private int Prev;
    void Start()
    {
        Prev = SceneManager.GetActiveScene().buildIndex -1;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            SceneManager.LoadScene(Prev);
        }
    }
}
