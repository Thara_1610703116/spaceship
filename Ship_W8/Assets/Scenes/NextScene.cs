﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
    public class NextScene : MonoBehaviour
    {
        private int next;
        void Start()
        {
            next = SceneManager.GetActiveScene().buildIndex + 1;
            //string name = SceneManager.GetActiveScene().name; 
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag == "Player")
            {
                print("next");
                SceneManager.LoadScene(next);
            }
        }
    }


